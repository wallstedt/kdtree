// Copyright (c) 2009-2014, Philip Wallstedt.  All rights reserved.
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//  * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include<iostream>
#include<fstream>
#include"kdtree.h"
using namespace std;

typedef size_t index;

const double machEps = std::numeric_limits<double>::epsilon();
const double huge = std::numeric_limits<double>::max();
const index nil = std::numeric_limits<index>::max();

typedef kdnode vec3;

inline double randDouble(double a_min, double a_max){
   long long int rmx=RAND_MAX;++rmx; // safe for -O3
   double normVal=double(rand()) * double(1./double(rmx)) * (a_max - a_min) ;
   return normVal + a_min;
}

void verifyRange(){
   const index N=1000;
   vector<index>cand,veri;
   vector<vec3>orig(N);
   for(index i=0;i<N;++i)orig[i]=vec3(randDouble(0.,1.),randDouble(0.,1.),randDouble(0.,1.));
   const double volPerNode=1./double(N); // unit cube/N
   const double l=2.*pow(volPerNode,1./double(3)); // range radius is Dth root
   // recursively insert the median point
   kdtree<vec3> tree;
   tree.insertMedianAll(orig);
   cout<<"Insert Calls: "<<tree.NinsertCalls()<<endl;
   for(index i=0;i<orig.size();++i){
      vec3 low =orig[i]-l;
      vec3 high=orig[i]+l;
      // perform kdtree range search
      tree.range(low,high,cand);
      sort(cand.begin(),cand.end());
      // perform brute force search
      veri.clear();
      for(index j=0;j<orig.size();++j)if(orig[j]>=low&&orig[j]<high)veri.push_back(j);
      sort(veri.begin(),veri.end());
      // results should be identical
      if(cand==veri)cout<<tree.NrangeCalls()<<' ';
      else cout<<"Failed range search\n";
   }
   cout<<endl;
}

void verifyFindNearest(){
   const index N=1000000;
   vector<index>cand,veri;
   vector<vec3>orig(N);
   for(index i=0;i<N;++i)orig[i]=vec3(randDouble(0.,1.),randDouble(0.,1.),randDouble(0.,1.));
   kdtree<vec3> tree;
   tree.insertMedianAll(orig);
   cout<<"Insert Calls: "<<tree.NinsertCalls()<<endl;
   for(index i = 0; i < 1000; ++i){
      const vec3 pt(randDouble(0.,1.),randDouble(0.,1.),randDouble(0.,1.));
      const index ki = tree.findNearest(pt);
      index vi = 0;
      double vr2 = len2(orig[vi] - pt);
      for(index i = 1; i < orig.size(); ++i){
         const double r2 = len2(orig[i] - pt);
         if(r2 < vr2){
            vi = i;
            vr2 = r2;
         }
      }
      if(ki == vi) cerr << '+';
      else cerr << "--@--";
      cerr << tree.NfindCalls() << ' ';
   }
   cout<<endl;
}

void speedFindNearest(){
   const index N=1000000;
   vector<index>cand,veri;
   vector<vec3>orig(N);
   for(index i=0;i<N;++i)orig[i]=vec3(randDouble(0.,1.),randDouble(0.,1.),randDouble(0.,1.));
   kdtree<vec3> tree;
   tree.insertMedianAll(orig);
   cout<<"Insert Calls: "<<tree.NinsertCalls()<<endl;
   const index Nsearch = 1000000;
   index Nfind = 0;
   for(index i = 0; i < Nsearch; ++i){
      const vec3 pt(randDouble(0.,1.),randDouble(0.,1.),randDouble(0.,1.));
      tree.findNearest(pt);
      Nfind += tree.NfindCalls();
   }
   cerr << " Average # of searches: " << Nfind / Nsearch << endl;
}

int main(){
   verifyRange();
   verifyFindNearest();
   //speedFindNearest();
}











