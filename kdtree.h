// Copyright (c) 2009-2014, Philip Wallstedt.  All rights reserved.
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//  * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef KDTREE_H
#define KDTREE_H
#include<vector>
#include<algorithm>
#include<cstdlib>
#include<iostream>
#include<cassert>
#include<cmath>
#include<limits>

// The user can define the kdtree with any node struct that has the required operations.
// The following node struct is provided as an example; the user may provide their own 
// node struct or inherit from this one.  The user supplies a node type when the kdtree 
// is defined.  For example: kdtree<userNode> myKdtree;
struct kdnode{
	typedef size_t index;
	double x, y, z;
	kdnode(){ x = 0.; y = 0.; z = 0.; }
	kdnode(const double&a){ x = a; y = a; z = a; }
	kdnode(const double a, const double b, const double c){ x = a; y = b; z = c; }
	kdnode(const kdnode&v){ x = v.x; y = v.y; z = v.z; }
	double&operator[](const index i){
		switch(i){
		case 0:return x;
		case 1:return y;
		case 2:return z;
		default: double *bad = new double(std::numeric_limits<double>::signaling_NaN()); return *bad;
		}
	}
	const double&operator[](const index i)const{
		switch(i){
		case 0:return x;
		case 1:return y;
		case 2:return z;
		default: double *bad = new double(std::numeric_limits<double>::signaling_NaN()); return *bad;
		}
	}
	const kdnode operator-(const kdnode&v)const{ return kdnode(x - v.x, y - v.y, z - v.z); }
	const kdnode operator+(const double&a)const{ return kdnode(x + a, y + a, z + a); }
	bool operator<=(const kdnode&v)const{ return x <= v.x&&y <= v.y&&z <= v.z; }
	bool operator>=(const kdnode&v)const{ return x >= v.x&&y >= v.y&&z >= v.z; }
	bool operator< (const kdnode&v)const{ return x< v.x&&y< v.y&&z< v.z; }
};


template<typename nodeT>
inline double len2(const nodeT&u){ return u.x*u.x + u.y*u.y + u.z*u.z; }


template<typename parentNodeT> class kdtree{
	typedef size_t index;

	// Each node in the kdtree is the median of a partition object
	class node :public parentNodeT{
		friend class kdtree;
		friend struct sortByDim;
		index lo; // index to lower partition
		index hi; // index to higher partition
		index id; // my index
		index dm; // dimension along which the partition is split
	public:
		// index() can be used to reorder the original part vector 'points'
		// which is known to dramatically improve efficiency of range searches
		// due to improved locality of points and higher cache hits
		index myID() const { return id; }
	};

	template<typename T>
	index dimOfMaxExtent(const T&x, const T&y, const T&z){
		index partDim = 0;
		T maxExt = x;
		if (y>maxExt){ partDim = 1; maxExt = y; }
		if (z>maxExt)partDim = 2;
		return partDim;
	}

	// Sorting condition used by nth_element
	// The < operator is tied to a chosen dimension
	struct sortByDim{
		const index dim;
		sortByDim(const index d) :dim(d){}
		sortByDim &operator=(const sortByDim &); // assignment not allowed
		bool operator()(const node&u, const node&v){ return u[dim]<v[dim]; }
	};

   std::vector<node>nodes;
   index root,Ninsert;
   mutable index Nrange, Nfind;
   mutable index NdimPartition[3];
public:
   index size()const{return nodes.size();}
   void clear(){ nodes.clear(); }
   const node operator[](const index i)const{return nodes[i];}
   index NinsertCalls()const{return Ninsert;}
   index NrangeCalls()const{return Nrange;}
   index NfindCalls()const{return Nfind;}
   void printPartition()const{ std::cerr << "Number of partitions in x, y, z: " << NdimPartition[0] << ", " << NdimPartition[1] << ", " << NdimPartition[2] << std::endl; }
   void reorder(std::vector<parentNodeT> &points)const{
      for(index i = 0; i < nodes.size(); ++i){
         points[i].x = nodes[i].x;
         points[i].y = nodes[i].y;
         points[i].z = nodes[i].z;
      }
   }

private:
	inline index findNearest(const parentNodeT &self, const index curr)const{
		++Nfind;
		const node &nd = nodes[curr];
		index best = nil;
		if(self[nd.dm] < nd[nd.dm]){
			if(nd.lo == nil) best = curr;
			else best = findNearest(self, nd.lo);
		}else{
			if(nd.hi == nil) best = curr;
			else best = findNearest(self, nd.hi);
		}

		if(len2(self - nodes[curr]) < len2(self - nodes[best])) best = curr;

		const double bestRad2 = len2(self - nodes[best]);
		const double bestRad = sqrt(bestRad2);
		index other = nil;
		if(self[nd.dm] < nd[nd.dm]){
			if( nd.hi != nil && bestRad > nd[nd.dm] - self[nd.dm] ) other = findNearest(self, nd.hi);
		}else{
			if( nd.lo != nil && bestRad > self[nd.dm] - nd[nd.dm] ) other = findNearest(self, nd.lo);
		}
		if(other != nil && len2(self - nodes[other]) < bestRad2) best = other;

		return best;
	}

public:
	inline index findNearest(const parentNodeT &self)const{
		Nfind = 0;
		return nodes[findNearest(self, root)].id;
	}

private:
	inline index insertMedian(const index start,  // first index of contiguous range of points
													 const index stop,   // last index of contiguous range of points
													 index ex,           // approximate relative sizes
													 index ey,
													 index ez){          // note that they are modified in this function!
		++Ninsert;
		if(start==stop)return nil; // nothing to partition; indicate that my parent is a leaf with 'nil'
		const index median=start+(stop-start-1)/2; // want to partition the nodes in two halves
		const index partDim=dimOfMaxExtent(ex,ey,ez);
		++NdimPartition[partDim];
		ex/=(partDim==0?2:1); // quick way to decide which dimension to split
		ey/=(partDim==1?2:1);
		ez/=(partDim==2?2:1);
		sortByDim op(partDim); // partitioning is performed along the 'partDim' dimension
		// In-place partition such that all points less than the median point have smaller
		// indices than 'median', and all points greater have greater indices than 'median'.
		// The operation is order O(n) and probably based on the Hoare algorithm.
		nth_element(nodes.begin()+start,nodes.begin()+median,nodes.begin()+stop,op);
		nodes[median].dm=partDim;                              // remember how we split this partition
		nodes[median].lo=insertMedian(start, median,ex,ey,ez); // Again partition the lower subinterval
		nodes[median].hi=insertMedian(median+1,stop,ex,ey,ez); // And partition the higher subinterval
		return median;
	}

public:
	inline void insertMedianAll(const std::vector<parentNodeT>&points){
		NdimPartition[0]=NdimPartition[1]=NdimPartition[2]=0;
		// fitting a box around the region so non-cubic particle clouds are handled efficiently
		parentNodeT lo( huge);
		parentNodeT hi(-huge);
		for(index i=0;i<points.size();++i){
			if(points[i].x>hi.x)hi.x=points[i].x;
			if(points[i].y>hi.y)hi.y=points[i].y;
			if(points[i].z>hi.z)hi.z=points[i].z;
			if(points[i].x<lo.x)lo.x=points[i].x;
			if(points[i].y<lo.y)lo.y=points[i].y;
			if(points[i].z<lo.z)lo.z=points[i].z;
		}
		const parentNodeT ext=hi-lo;
		const index partDim=dimOfMaxExtent(ext.x,ext.y,ext.z);
		++NdimPartition[partDim];
		const index ex=index(double(nil)*ext.x/ext[partDim]); // approximating the relative partition size with integers
		const index ey=index(double(nil)*ext.y/ext[partDim]);
		const index ez=index(double(nil)*ext.z/ext[partDim]);
		nodes.resize(points.size());
		for(index i=0;i<points.size();++i){
			nodes[i].id=i;
			nodes[i].x=points[i].x; // copy the points rather than reference them because
			nodes[i].y=points[i].y; // the points vector will be reordered in-place
			nodes[i].z=points[i].z;
		}
		Ninsert=0;
		root=insertMedian(0,points.size(),ex,ey,ez);
	}

private:
	inline void range(const parentNodeT&LO,             // low-left point of search box
									  const parentNodeT&HI,             // high-right point of search box
									  std::vector<index>&cand,     // place to put the indices that are located in the box
									  const index idx)const{  // index of current partition
		++Nrange;
		if(idx!=nil){
			if(nodes[idx]>=LO&&nodes[idx]<HI)cand.push_back(nodes[idx].id);                  // if in box, store index in cand
			if(nodes[idx][nodes[idx].dm]>=LO[nodes[idx].dm])range(LO,HI,cand,nodes[idx].lo); // if higher than LO, look in low partition
			if(nodes[idx][nodes[idx].dm]< HI[nodes[idx].dm])range(LO,HI,cand,nodes[idx].hi); // if lower than HI, look in high partition
		}
	}

public:
	inline void range(const parentNodeT&LO,               // low-left point of search box
									  const parentNodeT&HI,               // high-right point of search box
									  std::vector<index>&cand)const{ // place to put the indices that are located in the box
		Nrange=0;
		cand.clear();
		range(LO,HI,cand,root);
	}

};


#endif
